// le bouton est connecté à la broche A0 de la carte Adruino
const int bouton = A0;
// la LED à la broche 13
const int led = A1;

// variable qui enregistre l'état du bouton
int etatBouton;


void setup()
{
    pinMode(led, OUTPUT); // la led est une sortie
    pinMode(bouton, INPUT); // le bouton est une entrée
    etatBouton = LOW; // on initialise l'état du bouton
}

void loop()
{
    etatBouton = digitalRead(bouton);

    if(etatBouton == LOW) // test si le bouton a un niveau logique BAS
    {
        digitalWrite(led, LOW); //le bouton est relâché, la LED est éteinte
    }
    else  // test si le bouton a un niveau logique différent de HAUT
    {
        digitalWrite(led, HIGH); //la LED est allumé
    }
}
