#include "MMA7660.h"
MMA7660 accelemeter;

//Attribution de la LED, du Buzzer et du Bouton d'arret//
const int ledR = A1;
const int Buz = 6;
const int Button = A0;
//                  //

//Attribtion du capteur de toucher//
const int touch = 7;
//        //        //           //

//Déclarations des variables de l'accéleromètre//
float ax,ay,az;
//        //        //          //

//Déclarations variable//
boolean test = true;
//  //  //  //  //  //  //

//Déclaration des variables de temps//
unsigned long previousMillis=0 ;
unsigned long interval = 300000L;
//  //  //  //  //  //  //  //  //  //

//Déclaration Alarme activée//
boolean isOn = false;
int onTime = 0;
int cptOn = 0;

//  //  //  //  //  //  //

////////////////////////////////////////////////////////////////
//Fonction acceleromètre//
void accelerometre() {
  accelemeter.getAcceleration(&ax,&ay,&az);
}
//  //  //  //  //  //  //  //  //  //  //  //

//Fonction permet d'enclencher l'alarme aprés toucher ou acceleromètre mais aussi de l'arreter avec le bouton//
void alarme() {
  int toucher = digitalRead(touch);
  int appuyer = digitalRead(Button);
  
  if (ax > 1 || ay < -1 || az > 2 || toucher == HIGH){
  //for (int i = 0; i < 20; i ++){ //avec cette instruction l'alarme s'arrete au bout de 10 clignotement
    while (appuyer == LOW) { //l'alarme s'arrete seulemet si on appuye sur le bouton
      allumerLed();
      appuyer = digitalRead(Button);
    }
   isOn = true; 
  }
  else {
    digitalWrite(ledR, LOW);
    digitalWrite(Buz, LOW);
  }
}
///////Fonction allumage LED pour alarme/////////
void allumerLed(){ 
    digitalWrite(ledR, test);
    delay(200);
    //test = !test; ici le son est décalé par rapport à la LED
    digitalWrite(Buz, test);
    delay(200);
    test = !test; // ici le son est synchronisé avec la LED
}   
//  //  //  //  //  //  //  //  //  //  //  //



void setup() {
  //Initialisation de l'acceléromètre//
  accelemeter.init(); 
  //            //          //      //

  //Initialisation des LEDs//
  pinMode(Button, INPUT);
  pinMode(ledR, OUTPUT);
  pinMode(Buz, OUTPUT);
  //      //          //   //

  //Initialistion du capteur de toucher//
  pinMode(touch, INPUT);
  //      //    //    //    //  //    //
  Serial.begin(115200);
  
}

void loop() {
   if(onTime>0)
    {
        cptOn = 0;
        isOn = false;
        onTime--;
    }
    else
    {
    accelerometre();
    alarme();
    }
  }
